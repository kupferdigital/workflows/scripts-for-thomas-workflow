#!/bin/bash

printhelp () {
  echo "Usage: ./01.sh -i inputURL [-o outputPath] [-s serviceURL] [-e encoding] [-t seperator in input file] [-c ckanBaseURL] [-k ckanApiKey] [-p ckanPackageId]"
  echo "This script currently supports a csv file as an inputURL only"
  echo
  echo "Example usage: ./01.sh -i https://github.com/Mat-O-Lab/CSVToCSVW/raw/main/examples/example.csv"
  echo "Example usage uploading the result to ckan: ./01.sh -i https://github.com/Mat-O-Lab/CSVToCSVW/raw/main/examples/example.csv -c http://ckan.example.org -k apiKey -p packageId"
}

# TODO not tested yet
#fileContent to upload as a parameter, filename as a second parameter
function pushToCKAN {
  if [ -z "$ckanPackageId" ]; then
    echo "A package_id is mandatory to upload something to ckan. Use the -p option"
    exit 1;
  fi
  jq . <<< "$1" > /tmp/"$2"
  curl -H "Authorization: $ckanApiKey" "$ckanBaseURL/api/action/resource_create" --form upload=@/tmp/"$2" --form package_id="$ckanPackageId"
  rm /tmp/"$2"
}

if (($# == 0)); then
  echo "No arguments provided!"
  printhelp
  exit 1;
fi

while getopts hi:o:s:e:t:c:k:p: options ; do
 case "${options}" in
 o) outputPath=${OPTARG};;
 s) serviceURL=${OPTARG};;
 e) encoding=${OPTARG};;
 t) seperator=${OPTARG};;
 c) ckanBaseURL=${OPTARG};;
 k) ckanApiKey=${OPTARG};;
 p) ckanPackageId=${OPTARG};;
 i)
    if [ -n "$OPTARG" ] ; then
       inputURL=${OPTARG}
     else
       echo "No proper URL was specified!"
       exit 1 ;
     fi
     ;;
 h) printhelp; exit ;;
 *) exit;;
 \?) echo "Unknown option: -$OPTARG" >&2; exit 1 ;;
 esac
done

serviceURL=${serviceURL:-"http://csvtocsvw.kupferdigital.org"}
encoding=${encoding:-"auto"}
seperator=${seperator:-"auto"}

if ([ -z "$ckanBaseURL" ] && [ -n "$ckanApiKey" ]) || ([ -n "$ckanBaseURL" ] && [ -z "$ckanApiKey" ]) ; then
  echo "If you want to upload to a CKAN, all options, -c -k and -p must be used!"
  exit 1 ;
fi

date=$(date +'%Y%m%d')

currentNanoSeconds=$(date +%s%N)
http_response=$(curl -L -s -o response-"$currentNanoSeconds" -w "%{http_code}" -X POST "$serviceURL/api" -H "accept: application/json" -H "Content-Type: application/json" -d "{\"data_url\":\"$inputURL\", \"separator\": \"$seperator\",
  \"encoding\": \"$encoding\"}")

if [ "$http_response" != "200" ] ; then
    echo "$http_response"
    echo "Service wasn't able to process the data"
    cat response-"$currentNanoSeconds"
    rm response-"$currentNanoSeconds"
    exit 1;
else
    csvwJsonResult=$(cat response-"$currentNanoSeconds")
    rm response-"$currentNanoSeconds"

    filedata=$(echo "$csvwJsonResult" | jq -r '.filedata')

    filename=$(echo "$csvwJsonResult" | jq -r '.filename')
    extension=".json"
    filename=${filename%"$extension"}-"$date".json

    if [ -n "$outputPath" ] ; then
      jq . <<< "$filedata" > "$outputPath"/"$filename"
      echo "Wrote result to $outputPath/$filename"
    else
      result=$(jq . <<< "$filedata")
      echo "$result"
    fi

    if [ -n "$ckanBaseURL" ] && [ -n "$ckanApiKey" ]; then
      pushToCKAN "$filedata" "$filename"
    fi
fi
