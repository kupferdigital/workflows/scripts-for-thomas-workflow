#!/bin/bash

printhelp () {
  echo "Usage: ./02.sh [-m URL to mapping file] [-i URL to data file] [-o outputPath] [-s serviceURL] [-c ckanBaseURL] [-k ckanApiKey] [-p ckanPackageId]"
  echo "This script currently supports reading csv files and csvw jsonld mappings"
  echo
  echo "Example usage: ./02.sh -m https://github.com/Mat-O-Lab/MapToMethod/raw/main/examples/example-map.yaml -i ... "
}

# TODO not tested yet
#fileContent to upload and file ending as a parameter
function pushToCKAN() {
  if [ -z "$ckanPackageId" ]; then
    echo "A package_id is mandatory to upload something to ckan. Use the -p option"
    exit 1;
  fi
  currentNanoSeconds=$(date +%s%N)
  echo "$1" > /tmp/result-"$currentNanoSeconds"."$2"
  curl -H "Authorization: $ckanApiKey" "$ckanBaseURL/api/action/resource_create" --form upload=@/tmp/result-"$currentNanoSeconds"."$2" --form package_id="$ckanPackageId"
  rm /tmp/result-"$currentNanoSeconds"."$2"
}

if (($# == 0)); then
  echo "No arguments provided!"
  printhelp
  exit 1;
fi

while getopts hi:o:m:s:c:k:p: options ; do
 case "${options}" in
 o) outputPath=${OPTARG};;
 s) serviceURL=${OPTARG};;
 c) ckanBaseURL=${OPTARG};;
 k) ckanApiKey=${OPTARG};;
 p) ckanPackageId=${OPTARG};;
 i)
    if [ -n "$OPTARG" ] ; then
       dataURL=${OPTARG}
     else
       echo "No proper data URL was specified!"
       exit 1 ;
     fi
     ;;
 m)
   if [ -n "$OPTARG" ] ; then
      mappingURL=${OPTARG}
    else
      echo "No proper mapping URL was specified!"
      exit 1 ;
    fi
    ;;
 h) printhelp; exit ;;
 *) exit;;
 \?) echo "Unknown option: -$OPTARG" >&2; exit 1 ;;
 esac
done

# serviceURL=${serviceURL:-"http://rdfconverter.kupferdigital.org"}
serviceURL=${serviceURL:-"http://rdfconverter.metameat.xyz:8000"}

if ([ -z "$ckanBaseURL" ] && [ -n "$ckanApiKey" ]) || ([ -n "$ckanBaseURL" ] && [ -z "$ckanApiKey" ]) ; then
  echo "If you want to upload to a CKAN, all options, -c -k and -p must be used!"
  exit 1 ;
fi

if [ -z "$dataURL" ] || [ -z "$mappingURL" ] ; then
  echo "Option -i and -m is required!"
  exit 1 ;
fi

currentNanoSeconds=$(date +%s%N)

#create rml
http_response=$(curl -L -s -o response-"$currentNanoSeconds" -w "%{http_code}" -X POST "$serviceURL"/api/yarrrmltorml -H "accept: */*" -H "Content-Type: application/json" -d "{\"url\":\"$mappingURL\"}" )

if [ "$http_response" != "200" ] ; then
  echo "$http_response"
  echo "Service wasn't able to get or process the data"
  cat response-"$currentNanoSeconds"
  rm response-"$currentNanoSeconds"
  exit 1;
else
  resultingRML=$(cat response-"$currentNanoSeconds")
  echo "$resultingRML" > mapping-"$currentNanoSeconds".rml
  rm response-"$currentNanoSeconds"

  if [ -n "$ckanBaseURL" ] && [ -n "$ckanApiKey" ]; then
    pushToCKAN "$resultingRML" rml
  fi

  # create rdf
  http_response=$(curl -L -s -o response-"$currentNanoSeconds" -w "%{http_code}" -X POST "$serviceURL"/api/createrdf -H "accept: application/json" -H "Content-Type: application/json" -d "{\"mapping_url\": \"$mappingURL\", \"data_url\": \"\"}") # $dataURL
  if [ "$http_response" != "200" ] ; then
    echo "$http_response"
    echo "Service wasn't able to get or process the data"
    cat response-"$currentNanoSeconds"
    rm response-"$currentNanoSeconds"
    exit 1;
  else
    resultingRDF=$(cat response-"$currentNanoSeconds")
    rm response-"$currentNanoSeconds"

    #result = {
    #  "graph": "string",
    #  "num_mappings_applied": 0,
    #  "num_mappings_skipped": 0
    #}

    graphData=$(echo "$resultingRDF" | jq '.graph')
    numberOfAppliedMappings=$(echo "$resultingRDF"  | jq '.num_mappings_applied')
    numberOfSkippedMappings=$(echo "$resultingRDF"  | jq '.num_mappings_skipped')
    echo "Number of applied mappings: $numberOfAppliedMappings"
    echo "Number of skipped mappings: $numberOfSkippedMappings"
    if [ -n "$outputPath" ] ; then
      echo "$graphData" > "$outputPath"/result-"$currentNanoSeconds".ttl
      #unescape quots
      sed -i 's/\\"/'\"'/g' "$outputPath"/result-"$currentNanoSeconds".ttl # convert '\"' to proper '"'
      sed -i 's/"//' "$outputPath"/result-"$currentNanoSeconds".ttl # remove first '"'
      sed -i 's/\.\\n\\n"/\.\\n\\n/g' "$outputPath"/result-"$currentNanoSeconds".ttl # remove last '"'
      sed -i 's/\\n/\n/g' "$outputPath"/result-"$currentNanoSeconds".ttl # convert '\n' to real line breaks
    else
      echo "$graphData"
    fi

    if [ -n "$ckanBaseURL" ] && [ -n "$ckanApiKey" ]; then
      pushToCKAN "$graphData" rdf
    fi
  fi
fi
